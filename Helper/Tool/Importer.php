<?php

namespace Pcon\ObxImport\Helper\Tool;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Importer extends AbstractHelper
{
    const ATTRIBUTE_SET_ID = 4;

    /**
     * @var \Magento\Framework\Xml\Parser
     */
    protected $_xmlParser;

    /**
     * @var Data
     */
    protected $_helperData;

    public function __construct(
        \Magento\Framework\Xml\Parser $xmlParser,
        Data $helperData,
        Context $context
    )
    {
        $this->_helperData = $helperData;
        $this->_xmlParser = $xmlParser;
        parent::__construct($context);
    }

    public function getImportSettings($settings) {
        $importSettings = array(
            'import_action' => $settings['process'],
            'action_on_error' => $settings['errors'],
            'product_status'  => $settings['product_status'],
            'product_visibility' => $settings['product_visibility'],
            'product_tax_class' => $settings['product_tax_class'],
            'store_id'  => $settings['store']
        );

        return $importSettings;
    }

    public function getObxImportData($files, $settings) {
        $data = array();

        if(empty($files['obx'])) {
            return null;
        }
        
        $obxFile = $files['obx'];

        $obxXmlArray = $this->_xmlParser->load($obxFile)->xmlToArray();

        if(empty($obxXmlArray)) {
            return null;
        }

        $obxData = $obxXmlArray['cutBuffer']['items'];

        if(empty($obxData)) {
            return null;
        }
        
        $categories = array();
        if(!empty($obxData['bskFolder'])) {
            if(!empty($files['imgs'])) {
                $settings['product_imgs'] = $files['imgs'];
            }
            
            $rootCateId = (int) $this->_helperData->getRootCateIdByStore();
            
            $bskFolders = $obxData['bskFolder'];  // Categories
            $position = 0;
            
            foreach($bskFolders as $folder) {
                $categoryData = $this->_getCategoryObxData($folder, $rootCateId, $settings);
                
                $categories[] = $categoryData;
            }
        }
        
        return $categories;
    }
    
    protected function _getCategoryObxData($folder, $rootCateId, $settings) {
        $lang = $this->_helperData->getLocaleByStore($settings['store_id']);
        
        $folderData = $folder['_value'];
        
        // Basket ID
        $folderBasketId = $folder['_attribute']['basketId'];
        
        // Category Name
        if(isset($folderData['appData']['application']['_value']['artTextSet']['textGroup']['_value']['text'])) {
            $folderNameDatas = $folderData['appData']['application']['_value']['artTextSet']['textGroup']['_value']['text'];
        } else {
            $folderNameDatas = null;
        }
        
        if(!empty($folderNameDatas)) {
            foreach($folderNameDatas as $nameData) {
                if($nameData['_attribute']['lang'] == $lang) {
                    $categoryName = $nameData['_value'];
                } else {
                    $categoryName = $folderData['label'];
                }
            }
        } else {
            $categoryName = $folderData['label'];
        }
        
        $subCateData = null;
        if(!empty($folderData['bskFolder'])) {
            $subBskFolders = $folderData['bskFolder'];
            
            foreach($subBskFolders as $subFolder) {
                $subCateData[] = $this->_getCategoryObxData($subFolder, $rootCateId, $settings);
            }
        } else {
            $subCateData = null;
        }
        
        $products = null;       
        if(!empty($folderData['bskArticle'])) {
            $bskArticles = $folderData['bskArticle'];
            
            $products = $this->_getProductsObxData($bskArticles, $settings);
        }
        
        return array(
            'basketId' => $folderBasketId,
            'root_id' => $rootCateId,
            'name' => $categoryName,
            'sub' => $subCateData,
            'products' => $products
        );
    }
    
    protected function _getProductsObxData($productItems, $settings) {
        $lang = $this->_helperData->getLocaleByStore($settings['store_id']);
        $websiteId = (int) $this->_helperData->getWebsiteIdByStore($settings['store_id']);
        
        foreach ($productItems as $item) {
            $langFound = false;

            $series = $item['_value']['series'];
            $artSeries = '';                           // Art Series
            if(!empty($series['_attribute']['id'])) {
                $artSeries = $series['_attribute']['id'];
            }
            
            $name = "";                                 // Name via lang
            $nameDatas = $series['_value']['name'];
            if(!empty($nameDatas)) {
                if(!empty($nameDatas['_attribute'])) {
                    if($nameDatas['_attribute']['lang'] == $lang) {
                        $name = $nameDatas['_value'];
                        $langFound = true;
                    } else {
                        if($lang == "en" && trim($nameDatas['_attribute']['lang']) == "") {
                            $name = $nameDatas['_value'];
                            $langFound = true;
                        }
                    }
                } else {
                    
                    foreach ($nameDatas as $nameData) {
                        if($nameData['_attribute']['lang'] == $lang) {
                            $name = $nameData['_value'];
                            $langFound = true;
                        } else {
                            if($lang == "en" && trim($nameData['_attribute']['lang']) == "") {
                                $name = $nameData['_value'];
                                $langFound = true;
                            }
                        }
                    }
                }
            }
            
            $price = null;                                          // Price
            $priceGroup = $item['_value']['itemPrice'];
            foreach ($priceGroup as $priceData) {
                $priceData = $priceData['_attribute'];
                if($priceData['type'] == "sale" && empty($priceData['pd'])) {
                    $price = (float) $priceData['value'];
                }
            }

            $shortDescription = "";                                // Description
            $longDescription = "";
            $descriptionGroup = $item['_value']['description'];

            foreach ($descriptionGroup as $descriptionData) {
                $descriptionType = $descriptionData['_attribute']['type'];

                $descriptionDatas = $descriptionData['_value']['text'];

                $des = "";

                if(!empty($descriptionDatas['_attribute'])) {
                    if($descriptionDatas['_attribute']['lang'] == $lang) {
                        $des = $descriptionDatas['_value'];
                    } else {
                        if($lang == "en" && trim($descriptionDatas['_attribute']['lang']) == "") {
                            $des = $descriptionDatas['_value'];
                        }
                    }
                } else {
                    foreach ($descriptionDatas as $description) {
                        if($description['_attribute']['lang'] == $lang) {
                            $des = $description['_value'];
                        } else {
                            if($lang == "en" && trim($description['_attribute']['lang']) == "") {
                                $des = $description['_value'];
                            }
                        }
                    }
                }

                if($descriptionType == "short") {
                    $shortDescription = $des;
                }

                if($descriptionType == "long") {
                    $longDescription = $des;
                }
            }

            $basketId = '';                                          // Art
            if(!empty($item['_attribute']['basketId'])) {
                $basketId = $item['_attribute']['basketId'];
            }
            
            $imagePath = null;
            if($basketId != '') {
                if(isset($settings['product_imgs'])) {
                    $impImgs = $settings['product_imgs'];
                    foreach($impImgs as $img) {
                        if(strpos($img, $basketId) !== false) {
                            $imagePath = $img;
                        }
                    }
                }
            }

            $artBase = '';
            $artFinal = '';
            $sku = '';
            $artVarcode = '';
            $artOfmlVarcode = '';
            $artGroup = $item['_value']['artNr'];
            foreach ($artGroup as $art) {
                if($art['_attribute']['type'] == "base") {
                    $artBase = $art['_value'];         // Art Base
                }

                if($art['_attribute']['type'] == "final") {
                    $artFinal = $art['_value'];        // Art Final
                    $sku = $art['_value'];              // SKU
                }

                if($art['_attribute']['type'] == "varcode") {
                    $artVarcode = $art['_value'];      // Art Varcode
                }

                if($art['_attribute']['type'] == "ofmlvarcode") {
                    $artOfmlVarcode = $art['_value']; // Art OFML Varcode
                }
            }

            $data[] = array(
                'name'  => $name,
                'lang_found' => $langFound,
                'sku'   => $sku,
                'status'     => (int) $settings['product_status'],
                'visibility' => (int) $settings['product_visibility'],
                'attribute_set_id'  => self::ATTRIBUTE_SET_ID,
                'tax_class_id'  => (int) $settings['product_tax_class'],
                'price' => $price,
                'website_ids' => array($websiteId),
                'stock_data' => array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => 1,
                    'qty' => 10
                ),
                'image' => $imagePath,
                'short_description' => $shortDescription,
                'description'       => $longDescription,
                'art'        => $basketId,
                'art_series' => $artSeries,
                'art_base'   => $artBase,
                'art_final'  => $artFinal,
                'varcode'    => $artVarcode,
                'art_ofml_varcode'  => $artOfmlVarcode
            );
        }

        return $data;
    }
}