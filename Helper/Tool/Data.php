<?php

namespace Pcon\ObxImport\Helper\Tool;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    const XML_PATH_IMPORT_ENABLED   = "obximport/general/enabled";
    const XML_PATH_IMPORT_FILE_SIZE = "obximport/general/max_size";

    protected $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Context $context
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getLocaleByStore($storeId) {
        /**
         * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
         */
        $localeCode = $this->scopeConfig->getValue('general/locale/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        return explode('_', $localeCode)[0];
    }

    public function getWebsiteIdByStore($storeId) {
        $store = $this->_storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

        return $websiteId;
    }

    public function isEnableModule() {
        $enabled = (int) $this->scopeConfig->getValue(self::XML_PATH_IMPORT_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $enabled;
    }

    public function getMaxFileSizeConfig() {
        $fileSize = (int) $this->scopeConfig->getValue(self::XML_PATH_IMPORT_FILE_SIZE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $fileSize;
    }
    
    public function getRootCateIdByStore() {
        $store = $this->_storeManager->getStore();
        $rootNodeId = $store->getRootCategoryId();
        return $rootNodeId;
    }
}