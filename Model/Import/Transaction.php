<?php

namespace Pcon\ObxImport\Model\Import;

class Transaction
{
    protected $_transaction;

    protected $_product;
    
    protected $_category;

    protected $_importer;

    protected $_messageManager;

    protected $_importedCount = 0;

    protected $_missData = false;

    protected $_missProductsSKU = array();
    
    protected $_categoryLinkManagement;

    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Pcon\ObxImport\Helper\Tool\Importer $importer,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\Category $category,
        \Magento\Catalog\Model\CategoryLinkManagement $categoryLinkManagement,
        \Magento\Framework\DB\Transaction $transaction
    )
    {
        $this->_messageManager = $messageManager;
        $this->_importer = $importer;
        $this->_product = $product;
        $this->_category = $category;
        $this->_categoryLinkManagement = $categoryLinkManagement;
        $this->_transaction = $transaction;
    }

    public function executeImportData($settings, $files) {
        $importSettings = $this->_getImportSettings($settings);
        $actionOnError = $importSettings['action_on_error'];

        try {
            $result = $this->_processImport($importSettings, $files);
            if($result) {
                $this->_messageManager->addSuccess(__('%1 product(s) have been imported.', $this->_importedCount));
                $this->_messageManager->addNotice(
                    __('Please re-index and flush cache to update catalog data')
                );
            } else {
                if($actionOnError == "stop") {
                    $this->_messageManager->addError(__('Can\' get data from import file.'));
                }

                if($actionOnError == "skip") {
                    $this->_messageManager->addSuccess(__('%1 product(s) have been imported.', $this->_importedCount));
                    $this->_messageManager->addNotice(
                        __('Please re-index and flush cache to update catalog data')
                    );
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        } finally {
            $this->_importedCount = 0;
        }
    }

    protected function _processImport($settings, $files) {
        $importData = $this->_importer->getObxImportData($files, $settings);
        
        if(empty($importData)) {
            return false;
        }

        foreach ($importData as $index => $data) {
            $parentCate = $this->_category;
            $parentCate->load($data['root_id']);
            
            $this->_createCategory($data, $settings, $index, $parentCate);
        }

        if($this->_missData = true) {
            $this->_messageManager->addError(__('Some products can\'t get full data.'));

            $missSKUs = implode(', ', $this->_missProductsSKU);
            $this->_messageManager->addError(__('Product(s) which has SKU: %1 can\'t get full data. Please check the import file.', $missSKUs));
        }
        
        return true;
    }
    
    protected function _createCategory($data, $settings, $index, $parentCate) {
        $actionOnError = $settings['action_on_error'];
        $importAction = $settings['import_action'];
        
        $categoryModel = $this->_category;
        
        $parentCateId = $parentCate->getId();
        $parentCatePath = $this->_category->load($parentCateId)->getPath();
        
        $cateCheck = $categoryModel->getCollection()
                               ->addAttributeToFilter('parent_id', $parentCateId)
                               ->addAttributeToFilter('name', $data['name'])
                               ->getFirstItem();
                      
        if ($cateId = $cateCheck->getId()) {
            if($data['sub'] != null) {
                foreach($data['sub'] as $subIndex => $subData) {
                    $subCateCheck = $categoryModel->getCollection()
                           ->addAttributeToFilter('parent_id', $cateId)
                           ->addAttributeToFilter('name', $subData['name'])
                           ->getFirstItem();
                    
                    if($subCateId = $subCateCheck->getId()) {
                        if(!empty($subData['products'])) {
                            $productExisted = false;
                            $product = null;
                            
                            foreach($subData['products'] as $productData) {
                                if(!$productData['lang_found'] && $actionOnError == "stop") {
                                    $this->_missData = true;
                                    $this->_missProductsSKU[] = $productData['sku'];
                                    break;
                                }
                    
                                if(!$productData['lang_found'] && $actionOnError == "skip") {
                                    continue;
                                }
                    
                                if($this->_checkProductExisted($productData['sku'])) {
                                    $productExisted = true;
                                }
                    
                                if($importAction == "add_update") {
                                    if(!$productExisted) {
                                        $product = $this->_createProduct($productData);
                                    } else {
                                        $product = $this->_updateProduct($productData);
                                    }
                                }
                    
                                if($importAction == "add") {
                                    if(!$productExisted) {
                                        $product = $this->_createProduct($productData);
                                    }
                                }
                    
                                if($importAction == "update") {
                                    if($productExisted) {
                                        $product = $this->_updateProduct($productData);
                                    }
                                }
                    
                                if($product) {
                                    $this->_transaction->addObject($product)->save();
                                    $this->_assignProductToCategory($productData['sku'], array($subCateId));
                                    $this->_importedCount++;
                                }
                            }
                        } 
                    }
                
                }
            }
            
            if(!empty($data['products'])) {
                $productExisted = false;
                $product = null;
                
                foreach($data['products'] as $productData) {
                    if(!$productData['lang_found'] && $actionOnError == "stop") {
                        $this->_missData = true;
                        $this->_missProductsSKU[] = $productData['sku'];
                        break;
                    }
        
                    if(!$productData['lang_found'] && $actionOnError == "skip") {
                        continue;
                    }
        
                    if($this->_checkProductExisted($productData['sku'])) {
                        $productExisted = true;
                    }
        
                    if($importAction == "add_update") {
                        if(!$productExisted) {
                            $product = $this->_createProduct($productData);
                        } else {
                            $product = $this->_updateProduct($productData);
                        }
                    }
        
                    if($importAction == "add") {
                        if(!$productExisted) {
                            $product = $this->_createProduct($productData);
                        }
                    }
        
                    if($importAction == "update") {
                        if($productExisted) {
                            $product = $this->_updateProduct($productData);
                        }
                    }
        
                    if($product) {
                        $this->_transaction->addObject($product)->save();
                        $this->_assignProductToCategory($productData['sku'], array($cateId));
                        $this->_importedCount++;
                    }
                }
            } 
        } else {
            $category = $this->_category;
            
            $cateData = array(
                'name' => $data['name'],
                'parent_id' => $parentCateId,
                'path'  => $parentCatePath,
                'is_active' => true,
                'position' => $index
            );
            
            $category->setData($cateData);
    
            $this->_transaction->addObject($category)->save();
            
            $currentCateId = $category->getId();
            
            if($data['sub'] != null) {
                $parentId = $category->getId();
                foreach($data['sub'] as $subIndex => $subData) {
                    $curentCate = $this->_category;
                    $curentCate->load($parentId);
                    
                    $this->_createCategory($subData, $settings, $subIndex, $curentCate);
                }
            }
            
            if(!empty($data['products'])) {
                $productExisted = false;
                $product = null;
                
                foreach($data['products'] as $productData) {
                    if(!$productData['lang_found'] && $actionOnError == "stop") {
                        $this->_missData = true;
                        $this->_missProductsSKU[] = $productData['sku'];
                        break;
                    }
        
                    if(!$productData['lang_found'] && $actionOnError == "skip") {
                        continue;
                    }
        
                    if($this->_checkProductExisted($productData['sku'])) {
                        $productExisted = true;
                    }
        
                    if($importAction == "add_update") {
                        if(!$productExisted) {
                            $product = $this->_createProduct($productData);
                        } else {
                            $product = $this->_updateProduct($productData);
                        }
                    }
        
                    if($importAction == "add") {
                        if(!$productExisted) {
                            $product = $this->_createProduct($productData);
                        }
                    }
        
                    if($importAction == "update") {
                        if($productExisted) {
                            $product = $this->_updateProduct($productData);
                        }
                    }
        
                    if($product) {
                        $this->_transaction->addObject($product)->save();
                        $this->_assignProductToCategory($productData['sku'], array($currentCateId));
                        $this->_importedCount++;
                    }
                }
            }
            
            unset($category);
        }
    }

    protected function _createProduct($data, $type = "simple") {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->_product;

        $product->setData($data);
        $product->setTypeId($type);
        
        if($data['image'] != null) {
            $product->addImageToMediaGallery($data['image'], array('image', 'small_image', 'thumbnail'), false, false);
        }

        return $product;
    }

    protected function _updateProduct($data, $type = "simple") {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->_product;
        $productId = $this->_getProductIdBySku($data['sku']);

        $product->load($productId);
        $product->setData($data);
        $product->setId($productId);
        $product->setTypeId($type);
        
        if($data['image'] != null) {
            $product->addImageToMediaGallery($data['image'], array('image', 'small_image', 'thumbnail'), false, false);
        }

        return $product;
    }
    
    protected function _getProductIdBySku($sku) {
        $productId = $this->_product->getIdBySku($sku);
        if($productId) {
            return $productId;
        } else {
            return null;
        }
    }
    
    protected function _assignProductToCategory($sku, $cate_ids) {
        $this->_categoryLinkManagement->assignProductToCategories($sku, $cate_ids);
    }

    protected function _checkProductExisted($sku) {
        $existed = false;

        $productId = $this->_getProductIdBySku($sku);
        if($productId) {
            $existed = true;
        }

        return $existed;
    }

    protected function _getImportSettings($settings) {
        return $this->_importer->getImportSettings($settings);
    }
}