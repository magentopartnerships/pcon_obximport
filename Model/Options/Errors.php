<?php

namespace Pcon\ObxImport\Model\Options;

class Errors extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        // TODO: Implement toOptionArray() method.
        return [ "stop" => __('Stop import') , "skip" => __('Skip error & Continue import') ];
    }
}