<?php

namespace Pcon\ObxImport\Model\Options;

use \Magento\Store\Model\StoreRepository;

class Stores extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    protected $_storeRepository;

    protected $_storeManager;

    protected $_scopeConfig;

    public function __construct(
        StoreRepository $storeRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeRepository = $storeRepository;
        parent::__construct($data);
    }

    public function toOptionArray()
    {
        // TODO: Implement toOptionArray() method.
        $stores = $this->_storeManager->getStores($withDefault = false);
        $storeList = array();
        foreach ($stores as $store) {
            if ($store->getId() == 0) {
                continue;
            }
            $storeId = $store["store_id"];
            $storeName = $store["name"];
            $storeList[$storeId] = $storeName;
        }

        return $storeList;
    }
}