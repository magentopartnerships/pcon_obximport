<?php

namespace Pcon\ObxImport\Model\Options;

class Process extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        // TODO: Implement toOptionArray() method.
        return [ "add_update" => __('Add & Update') , "add" => __('Only Add New'), "update" => __('Only Update Existing')];
    }
}