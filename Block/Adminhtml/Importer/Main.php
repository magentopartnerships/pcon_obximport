<?php

namespace Pcon\ObxImport\Block\Adminhtml\Importer;

use Magento\Backend\Block\Template;

class Main extends Template
{
    public function __construct(Template\Context $context, array $data)
    {
        parent::__construct($context, $data);
    }
}