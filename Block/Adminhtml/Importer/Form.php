<?php

namespace Pcon\ObxImport\Block\Adminhtml\Importer;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{
    const DEFAULT_PRODUCT_STATUS = 1;
    const DEFAULT_PRODUCT_VISIBILITY = 4;
    const DEFAULT_PRODUCT_TAX_CLASS = 2;
    
    protected $_stores;

    protected $_process;

    protected $_errors;

    protected $_status;

    protected $_visibility;

    protected $_taxClasses;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Pcon\ObxImport\Model\Options\Stores $stores,
        \Pcon\ObxImport\Model\Options\Process $process,
        \Pcon\ObxImport\Model\Options\Errors $errors,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Magento\Tax\Model\TaxClass\Source\Product $taxClasses,
        array $data = []
    )
    {
        $this->_process = $process;
        $this->_errors = $errors;
        $this->_stores = $stores;
        $this->_status = $status;
        $this->_visibility = $visibility;
        $this->_taxClasses = $taxClasses;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'import_form', 'action' => $this->getUrl('pcon/tool/import'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );

        $form->setHtmlIdPrefix('importer_');

        $fieldset = $form->addFieldset(
            'setting_fieldset',
            ['legend' => __('Import Settings'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'process',
            'select',
            [
                'name' => 'process',
                'label' => __('Import Action'),
                'title' => __('Import Action'),
                'values' => $this->_process->toOptionArray(),
            ]
        );

        $fieldset->addField(
            'errors',
            'select',
            [
                'name' => 'errors',
                'label' => __('Action on Error'),
                'title' => __('Action on Error'),
                'values' => $this->_errors->toOptionArray(),
                'note' => __(
                    'Choose action when can\'t get full data of import file. <br> When get System error, stop and get error immediately.'
                )
            ]
        );

        $fieldset = $form->addFieldset(
            'product_fieldset',
            ['legend' => __('Product Settings'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'product_status',
            'select',
            [
                'name' => 'product_status',
                'label' => __('Status'),
                'title' => __('Status'),
                'value' => self::DEFAULT_PRODUCT_STATUS,
                'values' => $this->_status->toOptionArray(),
                'note' => __('Default is <b>Enable</b>')
            ]
        );

        $fieldset->addField(
            'product_visibility',
            'select',
            [
                'name' => 'product_visibility',
                'label' => __('Visibility'),
                'title' => __('Visibility'),
                'value' => self::DEFAULT_PRODUCT_VISIBILITY,
                'values' => $this->_visibility->toOptionArray(),
                'note' => __('Default is <b>Catalog, Search</b>')
            ]
        );

        $fieldset->addField(
            'product_tax_class',
            'select',
            [
                'name' => 'product_tax_class',
                'label' => __('Tax Class'),
                'title' => __('Tax Class'),
                'value' => self::DEFAULT_PRODUCT_TAX_CLASS,
                'values' => $this->_taxClasses->toOptionArray(),
                'note' => __('Default is <b>Taxable Goods</b>')
            ]
        );

        $fieldset = $form->addFieldset(
            'file_fieldset',
            ['legend' => __('File to Import'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'store',
            'select',
            [
                'name' => 'store',
                'label' => __('Choose Store'),
                'title' => __('Choose Store'),
                'values' => $this->_stores->toOptionArray(),
            ]
        );

        $fieldset->addField(
            'file',
            'file',
            [
                'name' => 'zip_file',
                'label' => __('File Upload'),
                'title' => __('File Upload'),
                'required' => true,
                'note' => __('Allowed File type is only \'.zip\'')
            ]
        );


        $fieldset->addField(
            'btn-submit',
            'submit',
            ['type' => 'submit', 'label' => '', 'class' => 'action-save action-secondary', 'value' => __('Import Data'), 'title' => __('Import Data')]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}