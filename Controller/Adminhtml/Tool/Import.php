<?php

namespace Pcon\ObxImport\Controller\Adminhtml\Tool;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Import extends Action
{
    const MIN_MB = 1 * 1024 * 1024;

    protected $_transaction;

    protected $_mediaDirectory;

    protected $_fileUploaderFactory;
    
    protected $_indexerFactory;
    
    protected $_indexerCollectionFactory;
    
    protected $_cacheManager;

    /**
     * @var \Pcon\ObxImport\Helper\Tool\Data
     */
    protected $_helperData;

    public function __construct(
        \Pcon\ObxImport\Model\Import\Transaction $transaction,
        \Pcon\ObxImport\Helper\Tool\Data $helper,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory,
        \Magento\Framework\App\Cache\Manager $cacheManager,
        Context $context
    )
    {
        $this->_helperData = $helper;
        $this->_transaction = $transaction;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::ROOT);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_indexerFactory = $indexerFactory;
        $this->_indexerCollectionFactory = $indexerCollectionFactory;
        $this->_cacheManager = $cacheManager;
        parent::__construct($context);
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $importSettings = $this->getRequest()->getPostValue();

        $file = $this->getRequest()->getFiles('zip_file');

        if($file['name'] !== "" && $file['size'] < ((int) $this->_helperData->getMaxFileSizeConfig() * self::MIN_MB)) {
            $zipFile = null;
            $importFiles = null;

            try {
                $target = $this->_mediaDirectory->getAbsolutePath('pub/import_files/');

                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'zip_file']);
                $uploader->setAllowedExtensions(['zip']);
                $uploader->setAllowCreateFolders(true);

                $result = $uploader->save($target);

                $zipFile = $result['path'] . $result['file'];
                
                $zipArchive = new \ZipArchive();

                $zipArchive->open($zipFile);
                
                $imageFiles = null;
                
                $obxFile = null;
                
                for ($i = 0; $i < $zipArchive->numFiles; $i++) {
                    $subFileName = $zipArchive->statIndex($i)['name'];
                    
                    // Check img files
                    if (strpos($subFileName, '.jpg') !== false || strpos($subFileName, '.png') !== false) {
                        $imageFiles[] = $subFileName;
                    }
                    
                    // Check obx file
                    if (strpos($subFileName, '.obx') !== false) {
                        $obxFile = $subFileName;
                    }
                }
                
                if($obxFile) {
                    $zipArchive->extractTo($target, $obxFile);
                    $importFiles['obx'] = $target . $obxFile;
                }
                
                if($imageFiles) {
                    $mediaPath = $this->_mediaDirectory->getAbsolutePath('pub/media/pcon_import/');
                    $zipArchive->extractTo($mediaPath, $imageFiles);
                    
                    foreach($imageFiles as $f) {
                        $importFiles['imgs'][] = $mediaPath . $f;
                    }
                }
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage()); 
                
                return $resultRedirect->setPath('pcon/importer/index');
            }

            $this->_transaction->executeImportData($importSettings, $importFiles);
            
            return $resultRedirect->setPath('pcon/importer/index');
        } else {
            $this->messageManager->addError(__('File to Import is missing or too large.'));
            
            return $resultRedirect->setPath('pcon/importer/index');
        }
    }
    
    protected function _reindex(){
        // list indexer
        $indexerCollection = $this->_indexerCollectionFactory->create();
        $indexerIds = $indexerCollection->getAllIds();
        
        // reindex
        foreach ($indexerIds as $indexerId){
            $indexer = $this->_indexerFactory->create();
            $indexer->load($indexerId);
            $indexer->reindexAll();
        }
    }
    
    protected function _flushCache()
    {
        $this->_cacheManager->flush($this->_cacheManager->getAvailableTypes());
    }
}