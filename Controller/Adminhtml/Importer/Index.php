<?php

namespace Pcon\ObxImport\Controller\Adminhtml\Importer;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var \Pcon\ObxImport\Helper\Tool\Data
     */
    protected $_helperData;

    public function __construct(
        \Pcon\ObxImport\Helper\Tool\Data $helper,
        PageFactory $pageFactory,
        Context $context
    )
    {
        $this->_helperData = $helper;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        if($this->_helperData->isEnableModule()) {
            $this->messageManager->addNotice(
                __('Make sure your file isn\'t more than %1M.', $this->_helperData->getMaxFileSizeConfig())
            );
        } else {
            $this->messageManager->addWarning(
                __('This module have been disabled')
            );
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $page = $this->_pageFactory->create();
        $page->setActiveMenu('Pcon_Base::catalog');
        $page->getConfig()->getTitle()->prepend(__('PCON Obx Importer'));

        return $page;
    }
}